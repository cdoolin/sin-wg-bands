#!/bin/bash

# set -x

if [ -e $1 ]; then
    echo expects k as argument
    exit 1
fi

k=$1
echo k $k

OUTDIR=sin-wg-ks-$k
OUTFILE=win-wg-ks-$k.out
KFILE=k-$k.kout

echo dir $OUTDIR
rm -rf ${OUTDIR}
mpirun -n 1 python3 sin-wg-bands.py -k $k -o ${OUTDIR} | tee ${OUTFILE}
grep k: ${OUTFILE} > $KFILE
grep harminv ${OUTFILE} >> $KFILE
cat $KFILE
