

calculate band structure of SiN waveguide on glass

mpi parrallism not efficient for large number of threads. quick benchmark of sim time vs num threads:

nthreads timestepping(s) communications(s)
1 70 15
2 40 13
4 30 12
6 30 11
8 60 25

Since we end up looping through many k vectors in simulation anyway, use simple multithreading by launching multiple simulations at once.

Can do it with xargs:

xargs -L1 -P4 ./run_ks.sh < k.dat

runs at most 4 processes (-P4) of ./run_ks.sh each with aguments from a line in k.dat.

made genks.py to generate k.dat from linspace.  or can manually edit k.dat



