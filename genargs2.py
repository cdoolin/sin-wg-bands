#!/usr/bin/env python3

from numpy import linspace, arange


hole_xs = arange(0, 0.6, 0.05)
hole_rs = arange(0, 0.4, 0.05)
ks = linspace(.3, .5, 21)


f = open("args.dat", 'w')

lines = 0
for i, hr in enumerate(hole_rs):
    for j, hx in enumerate(hole_xs):
        if hr == 0 and j > 0:
            continue

        for k in ks:
            f.write("--relhole-x %s --relhole-r %s --k %s\n" % (str(hx), str(hr), str(k)))
            lines += 1


f.close()

print("%d lines generated" % lines)
