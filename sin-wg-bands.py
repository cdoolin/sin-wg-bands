# following https://meep.readthedocs.io/en/latest/Python_Tutorials/Resonant_Modes_and_Transmission_in_a_Waveguide_Cavity/

import meep as mp
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('--outdir', '-o', type=str,
                    help='meep output directory')

parser.add_argument('--k', '-k', default=0.5, type=float, help='k vector')
                    
parser.add_argument('--hole-x', type=float, default=0, 
    help='hole displacement from center')
parser.add_argument('--hole-r', type=float, default=0, 
    help='hole displacement from center')
parser.add_argument('--fill', default=True, action='store_true', help="cut out between holes too")
parser.add_argument('--tinv', type=float, default=100, help="time to run harminv for")
parser.add_argument('--ntop', type=float, default=1, help="index of refraction top")
parser.add_argument('--nbot', type=float, default=1.44, help="index of refraction bottom")
parser.add_argument('--nwg', type=float, default=2., help="index of refraction waveguide")
parser.add_argument('--eps', default=False, action='store_true', help="only run saving epsilon")
parser.add_argument('--res', default=20, type=int, help="resolution of sim per unit length")
parser.add_argument('--wgwidth', default=1500, type=float, help="waveguide width (nm) default: 1500 nm")
parser.add_argument('--wgheight', default=220, type=float, help="waveguide height (nm) default: 1500 nm")
parser.add_argument('--a', '-a', default=500, type=float, help="unit cell length (nm) default: 500 nm")
parser.add_argument('--relhole-r', default=None, type=float, help="hole radius relative to unit cell length")
parser.add_argument('--relhole-x', default=None, type=float, help="hole radius relative to wg width")




args = parser.parse_args()



#
# Geometry
#


nm = 1. / args.a
a = 1. / nm * nm

dpml = 1000*nm

wg_w = args.wgwidth * nm
wg_h = args.wgheight * nm
space_w = 2000 * nm + dpml
space_h = 2000 * nm + dpml
hole_r = args.hole_r*nm # hole radius
if args.relhole_r is not None:
    hole_r = a * args.relhole_r

hole_x = args.hole_x*nm - hole_r

if args.relhole_x is not None:
    hole_x = wg_w * args.relhole_x - hole_r

if hole_x < 0:
    # hole_r > hole_x
    # shrink hole_r so that hole_x + hole_r is equal to args.hole_x
    hole_r = hole_x + hole_r
    hole_x = 0

total_w = wg_w + 2*space_w #+ 2*dpml
total_h = wg_h + 2*space_h #+ 2*dpml

mat_wg = mp.Medium(epsilon=args.nwg**2)
mat_top = mp.Medium(epsilon=args.ntop**2)
mat_bot = mp.Medium(epsilon=args.nbot**2)


resolution = args.res


# targs = ['args:', 'k', 'hole_x', 'hole_r', 'tinv', 'fill', 'ntop', 'nbot']
# print(", ".join(targs))
# targs = ['args:', args.k, args.hole_x, args.hole_r, args.tinv, int(args.fill), args.ntop, args.nbot]
# print(", ".join([str(a) for a in targs]))

dargs = {
    'k': args.k,
    'hole_x': hole_x / nm + hole_r / nm,
    'hole_r': hole_r / nm,
    'tinv': args.tinv,
    'fill': int(args.fill),
    'ntop': args.ntop,
    'nbot': args.nbot,
    'nwg': args.nwg,
    'a': a / nm,
    'wgwidth': wg_w / nm,
    'wgheight': wg_h / nm,
    'resolution': resolution,
}


targs = ["args:"] + [k for k, v in dargs.items()]
print(", ".join(targs))
targs = ["args:"] + [str(v) for k, v in dargs.items()]
print(", ".join(targs))



# print(args.relhole_x)
# import sys; sys.exit(1)


geometry = [
    # waveguide
    mp.Block(
        size=mp.Vector3(wg_w, a, wg_h), 
        material=mat_wg,
    ).shift(mp.Vector3(0, 0, 0)),

    # glass substrate
     mp.Block(
         size=mp.Vector3(total_w, a, space_h), 
         material=mat_bot,
     ).shift(mp.Vector3(0, 0, -space_h/2. - wg_h/2.)),
]

# add holes if needed
if hole_x > 0 and hole_r > 0:
    geometry += [
        mp.Cylinder(
            radius=hole_r, 
            height=wg_h, 
            material=mat_top,
        ).shift(mp.Vector3(hole_x, 0, 0)),

        mp.Cylinder(
            radius=hole_r, 
            height=wg_h, 
            material=mat_top,
        ).shift(mp.Vector3(-hole_x, 0, 0)),
    ]

    if args.fill:
        geometry += [
            mp.Block(
            size=mp.Vector3(2*hole_x, 2*hole_r, wg_h), 
            material=mat_top,
        ).shift(mp.Vector3(0, 0, 0)),
        ]

elif hole_r > 0:
    geometry += [
        mp.Cylinder(
            radius=hole_r, 
            height=wg_h, 
            material=mat_top,
        ).shift(mp.Vector3(0, 0, 0))
    ]


cell = mp.Vector3(total_w, 1, total_h)

#
#
#

#fcen = 1. / (1550*nm)  # pulse center frequency

k = .4
k_interp = 8

fcen = .4
df = .8     # pulse freq. width: large df = short impulse

print("fcen: %g" % fcen)
print("df: %g" % df)

src = [
    mp.Source(
        src=mp.GaussianSource(fcen, fwidth=df), 
        component=mp.Hy,
        center=mp.Vector3(0*nm, a*0.123, wg_h*0.4),
    ),
]

sym = [mp.Mirror(direction=mp.X, phase=-1)]

boundary = [
    mp.PML(dpml, direction=mp.X),
    mp.PML(dpml, direction=mp.Z),
]

sim = mp.Simulation(
    cell_size=cell, 
    geometry=geometry,
    sources=src,
    symmetries=sym,
    boundary_layers=boundary, 
    resolution=resolution
)

if args.outdir:
    sim.use_output_directory(args.outdir)
else:
    sim.use_output_directory()

print(cell * resolution)
size = cell*resolution
print(size[0])

# import sys; sys.exit(0)

#
# option A
# 
# kx = 0.4

# k = .38
# k = None
# k_interp = 10
k = args.k
print("k: %s" % str(k))

if args.eps:
    sim.run(
        mp.at_beginning(mp.output_epsilon),
        until=0,
    )
elif k is not None:
    sim.k_point = mp.Vector3(0, k, 0)

    sim.run(
        mp.at_beginning(mp.output_epsilon),

        # mp.at_every(5, 
        #     # mp.output_png(srccom, "-y 10 -Zc dkbluered -S 4")), 
        #     mp.output_png(mp.Ey, "-y 10 -Zc dkbluered -S 4")
        # ), 

        mp.after_sources(
            mp.Harminv(src[0].component, src[0].center, fcen, df)
        ),
        until_after_sources=args.tinv,
    )

    sim.run(
        mp.at_every(5, 
            # mp.output_png(srccom, "-y 10 -Zc dkbluered -S 4")), 
            mp.output_png(mp.Ey, "-y 10 -Zc dkbluered -S 4")
        ), 

        # until=1. / fcen,
        until=0,
    )

else:
    sim.run_k_points(
        100,
        mp.interpolate(
            k_interp, [
                mp.Vector3(0, 0, 0), 
                mp.Vector3(0, 0.5, 0)
            ])
    )