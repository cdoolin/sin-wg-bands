#!/usr/bin/env python3

from numpy import linspace, arange


hole_xs = [100]
hole_rs = [0, 50]
ks = linspace(.0, .5, 21)


f = open("args.dat", 'w')

lines = 0
for hx in hole_xs:
    for i, hr in enumerate(hole_rs):
        if hx == 0 and i > 0:
            pass
        for k in ks:
            f.write("--hole-x %s --hole-r %s --k %s\n" % (str(hx), str(hr), str(k)))
            lines += 1


f.close()

print("%d lines generated" % lines)
