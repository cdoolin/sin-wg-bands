#!/usr/bin/env python3



from numpy import linspace

import argparse
parser = argparse.ArgumentParser(description='')
parser.add_argument('start', type=float, help='start')
parser.add_argument('stop', type=float, help='stop')
parser.add_argument('npoints', type=int, help='npoints')

parser.add_argument('--out', '-o', type=str, default="args.dat", help='output file (default args.dat)')

args = parser.parse_args()

ks = linspace(args.start, args.stop, args.npoints)


f = open(args.out, 'w')

lines = 0
for k in ks:
    f.write("--k %s\n" % str(k))
    lines += 1
    

f.close()

print("%d lines generated" % lines)