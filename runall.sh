#!/bin/bash


AFILE=${AFILE:-args.dat}
# NPROC=${NPROC:-9}
NMPI=${NMPI:-1}

if [ -z $NPROC ]; then

NCPU=${NCPU:-$(nproc --all)}
echo $NCPU cores detected
(( NPROC = NCPU / NMPI ))

fi


echo $NMPI mpi threads per run
echo spawning $NPROC processes at once

# exit 1


echo afile $AFILE
# echo clargs $@
# exit 0

echo xargs -L1 -P${NPROC} ./run.sh $@ \< $AFILE
xargs -L1 -P${NPROC} ./run.sh $@ < $AFILE
