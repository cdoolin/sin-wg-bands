#!/bin/bash

# set -x

# if [ -e $1 ]; then
#     echo expects k as argument
#     exit 1
# fi

textargs=

for arg in "$@"; do
textargs=${textargs}-${arg//-}
done

NMPI=${NMPI:-1}

# k=$1
# echo k $k

OUTDIR=outdir${textargs}
OUTFILE=outfile${textargs}.out
AFILE=args${textargs}.aout

# echo dir ${OUTDIR}
rm -rf ${OUTDIR}
mpirun -n ${NMPI} python3 sin-wg-bands.py $@ -o ${OUTDIR} | tee ${OUTFILE}
grep args: ${OUTFILE} > $AFILE
grep harminv ${OUTFILE} >> $AFILE
cat $AFILE
