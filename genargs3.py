#!/usr/bin/env python3

from numpy import linspace, arange




wg_widths = linspace(-100, 100, 5) + 1200
hole_xs = linspace(-100, 100, 9) + 300
hole_rs = linspace(-50, 50, 5) + 120


f = open("args.dat", 'w')

lines = 0
for i, hr in enumerate(hole_rs):
    for j, hx in enumerate(hole_xs):

        for wgw in wg_widths:
            f.write("--hole-x %s --hole-r %s --wgwidth %s --k 0.5 --ntop 1 --nbot 1 --a 680\n" % (str(hx), str(hr), str(wgw)))
            lines += 1


f.close()

print("%d lines generated" % lines)
