#!/bin/bash

set -x

OUTDIR=sin-wg-bands-out-2

rm -rf ${OUTDIR}
mpirun -n 4 python3 sin-wg-bands.py -o ${OUTDIR} | tee ${OUTDIR}.out